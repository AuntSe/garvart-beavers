window.onload=function() {
	var wrapper = document.getElementById("wrapper");
	var header = document.getElementById("header");
	var content = document.getElementById("content");
	var container = document.getElementById("container");
	var footer = document.getElementById("footer");
	var river = document.getElementById("river");
	var logo = document.getElementById("logo");
	var beaver = document.getElementById("beaver");
	var body = document.getElementsByTagName("body");
	var docWidth, docHeight, wrapperWidth, wrapperHeight;

	window.addEventListener("resize", resizing);
	resizing();
	
	function resizing() {
		docWidth = 0
		docHeight = 0;
		wrapper.style.width = (0);
		wrapper.style.height = (0);
		footer.style.height = (0);
		river.style.height = (0);
		river.style.marginTop = (0);
		body[0].style.width = (0);
		body[0].style.height = (0);
		docWidth = document.documentElement.clientWidth;
		docHeight = document.documentElement.clientHeight;
		wrapper.style.width = (docWidth + "px");
		wrapper.style.height = (docHeight + "px");
		body[0].style.width = (docWidth + "px");
		body[0].style.height = (docHeight + "px");
		wrapperWidth = wrapper.offsetWidth;
		wrapperHeight = wrapper.offsetHeight;
		var sidesRelation;

		if (wrapperHeight<=wrapperWidth) {
			sidesRelation = wrapperWidth/wrapperHeight
			container.style.fontSize = (wrapperWidth*2.5/100 + "px");
			container.style.lineHeight = (wrapperWidth*3.8/100 + "px");
			if(sidesRelation>1.78) {
				footer.style.height = (wrapperHeight*3.8/100 + "px");
				river.style.height = (wrapperHeight*3.8/100 + "px");
				river.style.marginTop = (-wrapperHeight*3.8/100 + "px");
				logo.style.width = (53*wrapperHeight/100 + "px");
				beaver.style.width = (73*wrapperHeight/100 + "px");
				content.style.paddingTop = (2 + "%");
			}
			else {
				footer.style.height = (wrapperWidth*2.9/100 + "px");
				river.style.height = (wrapperWidth*2.9/100 + "px");
				river.style.marginTop = (-wrapperWidth*2.9/100 + "px");
				logo.style.width = (40 + "%");
				beaver.style.width = (55 + "%");
				content.style.paddingTop = (8 + "%");
			}
		}
		else {
			container.style.fontSize = (wrapperHeight*3.3/100 + "px");
			container.style.lineHeight = (wrapperHeight*5/100 + "px");	
			footer.style.height = (wrapperWidth*2.9/100 + "px");
			river.style.height = (wrapperWidth*2.9/100 + "px");
			river.style.marginTop = (-wrapperWidth*2.9/100 + "px");
			logo.style.width = (40 + "%");
			beaver.style.width = (55 + "%");
			content.style.paddingTop = (8 + "%");
		}	
	}	
}